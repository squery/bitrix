package bitrix

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

type OAuthResp struct {
	Access_token    string `json:"access_token"`
	Client_endpoint string `json:"client_endpoint"`
	Domain          string `json:"domain"`
	Expires_in      int    `json:"expires_in"`
	Member_id       string `json:"member_id"`
	Refresh_token   string `json:"refresh_token"`
	Scope           string `json:"scope"`
	Server_endpoint string `json:"server_endpoint"`
	Status          string `json:"status"`
	Error           string `json:"error"`
	Error_desc      string `json:"error_description"`
}

func (c *Client) Auth(w http.ResponseWriter, r *http.Request, f func(*OAuthResp, error)) {
	if code := r.FormValue("code"); code == "" {
		domain := r.FormValue("DOMAIN")
		http.Redirect(w, r, "https://"+domain+"/oauth/authorize/?client_id="+c.Client_id, http.StatusTemporaryRedirect)
	} else {
		f(c.getAccess(code))
	}
}

func (c *Client) getAccess(code string) (*OAuthResp, error) {
	params := map[string]string{
		"grant_type":    "authorization_code",
		"client_id":     c.Client_id,
		"client_secret": c.Client_secret,
		"code":          code,
	}

	return c.oauthQuery(params)
}

func (c *Client) Refresh(refresh_token string) (*OAuthResp, error) {
	params := map[string]string{
		"grant_type":    "refresh_token",
		"client_id":     c.Client_id,
		"client_secret": c.Client_secret,
		"refresh_token": refresh_token,
	}
	return c.oauthQuery(params)
}

func (c *Client) oauthQuery(params map[string]string) (*OAuthResp, error) {
	base_url := "https://oauth.bitrix.info/oauth/token/"

	v := url.Values{}
	for key, val := range params {
		v.Add(key, val)
	}
	resp, err := http.Get(base_url + "?" + v.Encode())
	if err != nil {
		return nil, errors.New(fmt.Sprintf("oauth query error: %s", err))
	}

	bts, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("oauth query error: %s", err))
	}

	r := &OAuthResp{}
	err = json.Unmarshal(bts, r)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("oauth query error: %s", err))
	}

	if r.Error != "" || r.Error_desc != "" {
		return nil, errors.New(fmt.Sprintf("oauth query error: %s: %s", r.Error, r.Error_desc))
	}

	return r, nil
}
