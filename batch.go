package bitrix

import (
	"encoding/json"
	"errors"
	"fmt"
)

var (
	ErrBigLength = errors.New("batch length more than 50")
)

type Batch struct {
	client *Client

	Halt int64             `json:"halt"`
	Cmd  map[string]string `json:"cmd"`
}

func NewBatch(client *Client) *Batch {
	return &Batch{
		client: client,
		Halt:   0,
		Cmd:    make(map[string]string, 0),
	}
}

func (b *Batch) Add(name, query string) {
	b.Cmd[name] = query
}

func (b *Batch) Len() int {
	return len(b.Cmd)
}

func (b *Batch) Clear() {
	for key, _ := range b.Cmd {
		delete(b.Cmd, key)
	}
}

type RespBatch map[string]json.RawMessage

func newRespBatch(length int) RespBatch { return make(map[string]json.RawMessage, length) }

type respBatchBitrix struct {
	Result struct {
		Result       json.RawMessage `json:"result"`
		ResultErrors json.RawMessage `json:"result_error"`
	} `json:"result"`
}

func (b *Batch) Method(domain, accessToken string) (RespBatch, error) {
	if b.Len() > 50 {
		return newRespBatch(0), ErrBigLength
	}
	if b.Len() == 0 {
		return newRespBatch(0), nil
	}

	ok, body, err := b.client.Method(domain, accessToken, "batch", b)
	if err != nil {
		return newRespBatch(0), err
	}
	if !ok {
		return newRespBatch(0), nil
	}

	return parseBatch(body, b.Len())
}

func parseBatch(body []byte, length int) (RespBatch, error) {
	resp := new(respBatchBitrix)

	err := json.Unmarshal(body, resp)
	if err != nil {
		return newRespBatch(0), fmt.Errorf("parse batch error: %w", err)
	}
	respBatch := newRespBatch(length)
	if len(resp.Result.Result) != 0 && resp.Result.Result[0] == '{' {
		json.Unmarshal(resp.Result.Result, &respBatch)
	}

	if len(resp.Result.ResultErrors) != 0 && resp.Result.ResultErrors[0] == '{' {
		json.Unmarshal(resp.Result.ResultErrors, &respBatch)
	}

	return respBatch, nil
}
