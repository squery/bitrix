package bitrix

import (
	"bytes"
	"encoding/json"
	"fmt"
	"testing"
)

// func TestBatchAdd(t *testing.T) {
// 	list := []struct {
// 		cmd      map[string]string
// 		expected map[string]string
// 	}{
// 		{
// 			map[string]string{
// 				"app":     "app.info",
// 				"transit": "crm.deal.userfield.list?filter[FIELD_NAME]=UF_CRM_TRK_TRANSIT",
// 				"total":   "crm.deal.list?select[]=TITLE&filter[!UF_CRM_TRK_TRACKER]=&filter[!UF_CRM_TRK_TRANSIT]=&filter[!UF_CRM_TRK_STATUS_B]=200 - доставлено&filter[!UF_CRM_TRK_STATUS_E]=[\"ПР | Возврат: Иные обстоятельства | 3.9\"]",
// 			},
// 			map[string]string{
// 				"app":     "app.info",
// 				"transit": "crm.deal.userfield.list%3Ffilter%5BFIELD_NAME%5D%3DUF_CRM_TRK_TRANSIT",
// 				"total":   "crm.deal.list%3Fselect%5B%5D%3DTITLE%26filter%5B%21UF_CRM_TRK_TRACKER%5D%3D%26filter%5B%21UF_CRM_TRK_TRANSIT%5D%3D%26filter%5B%21UF_CRM_TRK_STATUS_B%5D%3D200+-+%D0%B4%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%BE%26filter%5B%21UF_CRM_TRK_STATUS_E%5D%3D%5B%22%D0%9F%D0%A0+%7C+%D0%92%D0%BE%D0%B7%D0%B2%D1%80%D0%B0%D1%82%3A+%D0%98%D0%BD%D1%8B%D0%B5+%D0%BE%D0%B1%D1%81%D1%82%D0%BE%D1%8F%D1%82%D0%B5%D0%BB%D1%8C%D1%81%D1%82%D0%B2%D0%B0+%7C+3.9%22%5D",
// 			},
// 		},
// 	}
// 	for _, l := range list {
// 		batch := NewBatch(nil)
// 		for key, value := range l.cmd {
// 			batch.Add(key, value)
// 		}
// 		if len(l.cmd) != batch.Len() {
// 			t.Error("length not equal")
// 		}
// 		for key, value := range batch.Cmd {
// 			if l.expected[key] != value {
// 				t.Error("value not equal")
// 			}
// 		}
// 	}

// }

func TestParseBatch(t *testing.T) {
	list := []struct {
		body     []byte
		expected map[string]json.RawMessage
	}{
		{
			[]byte(`{"result":{"result":{"app":{"ID":"19","CODE":"local.6082c53663ca88.84015544","VERSION":1,"STATUS":"L","INSTALLED":true,"PAYMENT_EXPIRED":"N","DAYS":null,"LANGUAGE_ID":"ru","LICENSE":"ru_nfr","LICENSE_TYPE":"nfr","LICENSE_FAMILY":"nfr"},"transit":[{"ID":"219","ENTITY_ID":"CRM_DEAL","FIELD_NAME":"UF_CRM_TRK_TRANSIT","USER_TYPE_ID":"enumeration","XML_ID":"UF_CRM_TRK_TRANSIT","SORT":"100","MULTIPLE":"N","MANDATORY":"N","SHOW_FILTER":"N","SHOW_IN_LIST":"Y","EDIT_IN_LIST":"Y","IS_SEARCHABLE":"N","SETTINGS":{"DISPLAY":"LIST","LIST_HEIGHT":1,"CAPTION_NO_VALUE":"","SHOW_NO_VALUE":"Y"},"LIST":[{"ID":"45","SORT":"500","VALUE":"\u041f\u043e\u0447\u0442\u0430 \u0420\u043e\u0441\u0441\u0438\u0438","DEF":"N"},{"ID":"47","SORT":"500","VALUE":"\u0421\u0414\u042d\u041a","DEF":"N"},{"ID":"49","SORT":"500","VALUE":"\u0414\u0435\u043b\u043e\u0432\u044b\u0435 \u043b\u0438\u043d\u0438\u0438","DEF":"N"},{"ID":"51","SORT":"500","VALUE":"Boxberry","DEF":"N"},{"ID":"53","SORT":"500","VALUE":"\u041f\u042d\u041a","DEF":"N"},{"ID":"55","SORT":"500","VALUE":"DPD","DEF":"N"},{"ID":"57","SORT":"500","VALUE":"GTD (KIT)","DEF":"N"},{"ID":"59","SORT":"500","VALUE":"IML","DEF":"N"}]}]},"result_error":{"tasks":{"error":22002,"error_description":"Could not find description of lis in Bitrix\\Tasks\\Rest\\Controllers\\Task"}},"result_total":{"transit":1},"result_next":[],"result_time":{"app":{"start":1619786533.5883629,"finish":1619786533.589535,"duration":0.0011720657348632812,"processing":0.0011160373687744141,"date_start":"2021-04-30T15:42:13+03:00","date_finish":"2021-04-30T15:42:13+03:00"},"transit":{"start":1619786533.5908899,"finish":1619786533.6000941,"duration":0.0092041492462158203,"processing":0.0091660022735595703,"date_start":"2021-04-30T15:42:13+03:00","date_finish":"2021-04-30T15:42:13+03:00"}}},"time":{"start":1619786533.563323,"finish":1619786533.6001151,"duration":0.03679203987121582,"processing":0.011788845062255859,"date_start":"2021-04-30T15:42:13+03:00","date_finish":"2021-04-30T15:42:13+03:00"}}`),

			map[string]json.RawMessage{
				"app":     json.RawMessage(`{"ID":"19","CODE":"local.6082c53663ca88.84015544","VERSION":1,"STATUS":"L","INSTALLED":true,"PAYMENT_EXPIRED":"N","DAYS":null,"LANGUAGE_ID":"ru","LICENSE":"ru_nfr","LICENSE_TYPE":"nfr","LICENSE_FAMILY":"nfr"}`),
				"transit": json.RawMessage(`[{"ID":"219","ENTITY_ID":"CRM_DEAL","FIELD_NAME":"UF_CRM_TRK_TRANSIT","USER_TYPE_ID":"enumeration","XML_ID":"UF_CRM_TRK_TRANSIT","SORT":"100","MULTIPLE":"N","MANDATORY":"N","SHOW_FILTER":"N","SHOW_IN_LIST":"Y","EDIT_IN_LIST":"Y","IS_SEARCHABLE":"N","SETTINGS":{"DISPLAY":"LIST","LIST_HEIGHT":1,"CAPTION_NO_VALUE":"","SHOW_NO_VALUE":"Y"},"LIST":[{"ID":"45","SORT":"500","VALUE":"\u041f\u043e\u0447\u0442\u0430 \u0420\u043e\u0441\u0441\u0438\u0438","DEF":"N"},{"ID":"47","SORT":"500","VALUE":"\u0421\u0414\u042d\u041a","DEF":"N"},{"ID":"49","SORT":"500","VALUE":"\u0414\u0435\u043b\u043e\u0432\u044b\u0435 \u043b\u0438\u043d\u0438\u0438","DEF":"N"},{"ID":"51","SORT":"500","VALUE":"Boxberry","DEF":"N"},{"ID":"53","SORT":"500","VALUE":"\u041f\u042d\u041a","DEF":"N"},{"ID":"55","SORT":"500","VALUE":"DPD","DEF":"N"},{"ID":"57","SORT":"500","VALUE":"GTD (KIT)","DEF":"N"},{"ID":"59","SORT":"500","VALUE":"IML","DEF":"N"}]}]`),
				"tasks":   json.RawMessage(`{"error":22002,"error_description":"Could not find description of lis in Bitrix\\Tasks\\Rest\\Controllers\\Task"}`),
			},
		},
		{
			[]byte(`{"result":{"result":[],"result_error":{"100":{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"},"101":{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"},"102":{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"},"103":{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"},"104":{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"},"105":{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"},"106":{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"},"107":{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"},"108":{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"}},"result_next":[],"result_time":[],"result_total":[]},"time":{"date_finish":"2021-05-12T16:37:23+03:00","date_start":"2021-05-12T16:37:23+03:00","duration":0.023911952972412109,"finish":1620826643.7055199,"processing":0.0014009475708007812,"start":1620826643.681608}}`),

			map[string]json.RawMessage{
				"100": json.RawMessage(`{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"}`),
				"101": json.RawMessage(`{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"}`),
				"102": json.RawMessage(`{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"}`),
				"103": json.RawMessage(`{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"}`),
				"104": json.RawMessage(`{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"}`),
				"105": json.RawMessage(`{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"}`),
				"106": json.RawMessage(`{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"}`),
				"107": json.RawMessage(`{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"}`),
				"108": json.RawMessage(`{"error":"ERROR_METHOD_NOT_FOUND","error_description":"Method not found!"}`),
			},
		},
	}
	for _, l := range list {
		respBatch := parseBatch(l.body, len(l.expected))
		if len(l.expected) != len(respBatch) {
			t.Error("length not equal")
		}
		for key, value := range respBatch {
			if !bytes.Equal(l.expected[key], value) {
				fmt.Println("expected: ", string(l.expected[key]))
				fmt.Println("real: ", string(value))
				t.Error("value not equal")
			}
		}
	}
}
